import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean{
    if (this.authService.isLoggedIn()) 
      return true;
    
    this.router.navigate(["/login"], {queryParams: {returnUrl: state.url}}); // state.url represents the current location before the app sends us to the login page when a user wasn't authenticated
    return false;
    
  }
}
