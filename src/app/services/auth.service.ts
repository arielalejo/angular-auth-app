import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { JwtHelperService } from "@auth0/angular-jwt";

const url = 'http://localhost:3001/users';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

    login(credentials: any): Observable<any> {
        return this.http.post<any>(url, credentials).pipe(
            map((response) => {
                if (response && response.token) {
                    localStorage.setItem('token', response.token);
                    return true;
                }
                return false;
            }),
            catchError((error) => {
                console.log('service error:', error);
                return throwError(false); // returns false in the error block of the suscriber of this service
            })
        );
    }

    logout(){
        localStorage.removeItem("token");
    }

    isLoggedIn(): boolean{                       
        let isExpired = this.jwtHelper.isTokenExpired();

        return !isExpired;
    }

    get currentUser(){
        if(!this.isLoggedIn()) return null;

        return this.jwtHelper.decodeToken();
    }

    getToken(): string|null{
        return localStorage.getItem("token");
    }
}
