import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

const url = "http://localhost:3001/courses"

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient, private authService:AuthService) { }

  getCourses(): Observable<number[]>{
    const token = this.authService.getToken() as string;
    
    return this.http.get<number[]>(url, {headers: {'x-auth-token': token}});
  }
}
