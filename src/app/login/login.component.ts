import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    invalidLogin: boolean = false;
    form: FormGroup;

    constructor(
        fb: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.form = fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    ngOnInit(): void {}

    login(credentials: any): void {

        this.authService.login(credentials).subscribe(
            (response) => {
                if (response) {
                    console.log('login:', response);
                    this.invalidLogin = false;

                    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    this.router.navigate([returnUrl || '/']);
                }
            },
            (error) => {
                console.log('login error:', error); // it should be false
                this.invalidLogin = true;
            }
        );

        this.form.reset();
    }
}
