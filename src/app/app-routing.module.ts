import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NotAccessComponent } from './not-access/not-access.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdminGuardService } from './services/admin-guard.service';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {path: "", component:HomeComponent},
  {path: "login", component: LoginComponent},
  {path: "admin", component: AdminComponent, canActivate: [AuthGuardService, AdminGuardService]},
  {path: "not-access", component: NotAccessComponent},
  {path: "**", component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
